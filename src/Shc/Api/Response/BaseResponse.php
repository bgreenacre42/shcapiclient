<?php namespace Shc\Api\Response;

use Guzzle\Service\Command\OperationCommand;
use Guzzle\Service\Command\ResponseClassInterface;

/**
 * Class to represent responses returned from the Clickatell API.
 */
class BaseResponse implements ResponseClassInterface {
    /**
     * @var array parsed elements of the response
     */
    public $parsedResponse;

    /**
     * @var array contains some api tracking data.
     */
    protected $apiTracking = array();

    /**
     * Create an object to handle response from api.
     * 
     * @param  OperationCommand $command The guzzle command for the api.
     * @return object                    Instance of this class.
     */
    public static function fromCommand(OperationCommand $command)
    {
        $class = $command->getOperation()->getResponseClass();

        switch(strtolower($command->getRequest()->getQuery()->get('contentType')))
        {
            case 'xml':
                $parsedResponse = $command->getRequest()->getResponse()->xml();

                break;
            case 'json':
            default:
                $parsedResponse = $command->getRequest()->getResponse()->json();

                break;
        }

        return new $class($command->getRequest()->getResponse()->json());
    }

    /**
     * Create a new response object from a Clickatell request and response.
     *
     * @param \Guzzle\Http\Message\RequestInterface $request The request object 
     * associated with the response.
     * @throws \UnexpectedValueException when the request does not have a response.
     */
    public function __construct($data)
    {
        $this->parseBody($data);

        unset($data);
    }

    public function __destruct()
    {
        $this->parsedResponse = null;
        $this->apiTracking = null;
    }

    /**
     * Used to determine if the response is successful.
     */
    public function isSuccessful()
    {
        return true;
    }

    /**
     * Get details of the error returned from the API.
     *
     * @return false|Error
     */
    public function getError()
    {
        return null;
    }

    /**
     * Parse the body of the response.
     * 
     * @param string $body Response body
     * @return array parts matched in the response body
     */
    protected function parseBody($parsedResponse)
    {
        // Grab any api tracking metrics
        if (isset($parsedResponse['ApiTracking']))
        {
            $data = array_pop($parsedResponse['ApiTracking']);

            foreach (explode('|', $data) as $parts)
            {
                $parts = preg_split('/\s?\:\s?/', $parts);

                $this->apiTracking[$parts[0]] = $parts[1];
            }
        }

        // Figuring out which root to set parsedResponse too.
        if (isset($parsedResponse['mercadoresult']))
        {
            $this->parsedResponse = $parsedResponse['mercadoresult'];
        }
        elseif (isset($parsedResponse['productdetail']))
        {
            $this->parsedResponse = $parsedResponse['productdetail'];
        }
    }

}
