<?php namespace Shc\Api\Response;

class ProductSearch extends BaseResponse {

    protected $products = array();

    public function __destruct()
    {
        parent::__destruct();

        $this->products = null;
    }

    /**
     * Get the array of departments from the
     * response object.
     * 
     * @return array
     */
    public function getDepartments()
    {
        return (isset($this->parsedResponse['departments']['department'][1]))
            ? $this->parsedResponse['departments']['department'][1]
            : array();
    }

    /**
     * Get the nav groups.
     * 
     * @return array
     */
    public function getNavGroups()
    {
        return (isset($this->parsedResponse['navgroups']['navgroup'][1]))
            ? $this->parsedResponse['navgroups']['navgroup'][1]
            : array();
    }

    /**
     * Get the categories array.
     * 
     * @return array
     */
    public function getCategories()
    {
        if (isset($this->parsedResponse['categories']['category'][1]))
        {
            return $this->parsedResponse['categories']['category'][1];
        }
        elseif (isset($this->parsedResponse['featuredcategories']['featuredcategory'][1]))
        {
            return $this->parsedResponse['featuredcategories']['featuredcategory'][1];
        }
        elseif (isset($this->parsedResponse['featuredsubcategories']['featuredsubcategory'][1]))
        {
            return $this->parsedResponse['featuredsubcategories']['featuredsubcategory'][1];
        }
        else
        {
            return array();
        }
    }

    /**
     * Get the found products.
     * 
     * @return array
     */
    public function getProducts()
    {
        if (empty($this->products) && isset($this->parsedResponse['products']['product'][1]))
        {
            $this->products = $this->parsedResponse['products']['product'][1];
        }

        return $this->products;
    }

    /**
     * Get the filtered ranks array.
     * 
     * @return array
     */
    public function getFilterRanks()
    {
        return (isset($this->parsedResponse['filterranks']['filterrank'][1]))
            ? $this->parsedResponse['filterranks']['filterrank'][1]
            : array();
    }

    /**
     * Get the total product found count based
     * on the search criteria given.
     * 
     * @return integer The total product count.
     */
    public function getTotalCount()
    {
        return (isset($this->parsedResponse['productcount']))
            ? (int) $this->parsedResponse['productcount']
            : 0;
    }

}