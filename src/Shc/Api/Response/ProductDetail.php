<?php namespace Shc\Api\Response;

class ProductDetail extends BaseResponse {

    /**
     * @var array Sane Product detail array.
     */
    protected $product;

    /**
     * @var array Insane Product detail array.
     */
    protected $detail;

    public function __destruct()
    {
        parent::__destruct();

        $this->detail = $this->product = null;
    }

    /**
     * After the body is parsed into an array, setup
     * internal properties of this object.
     * 
     * @param  object $response Response object
     * @return void
     */
    public function parseBody($response)
    {
        parent::parseBody($response);

        $this->detail = (isset($this->parsedResponse['softhardproductdetails'][1][0]))
            ? $this->parsedResponse['softhardproductdetails'][1][0]
            : array();

        $this->setupProduct();

        if (count($this->product) > 0)
        {
            $this->setupAttributes();
            $this->setupSpecifications();
        }

        $this->detail = null;
    }

    /**
     * Was the request successful.
     * 
     * @return boolean True if it was successful else False.
     */
    public function isSuccessful()
    {
        return (isset($this->parsedResponse['statusdata']['responsecode']) && $this->parsedResponse['statusdata']['responsecode'] == 0)
            ? true
            : false;
    }

    /**
     * Get any error message given by the API response.
     * 
     * @return string Error message.
     */
    public function getError()
    {
        return (isset($this->parsedResponse['statusdata']['respmessage']))
            ? $this->parsedResponse['statusdata']['respmessage']
            : null;
    }

    /**
     * Build the Sane Product detail array.
     * 
     * @return void
     */
    protected function setupProduct()
    {
        if ( ! $this->isSuccessful() || ! isset($this->detail['brandname']) || empty($this->detail['regularprice']))
        {
            return;
        }

        $this->product = array(
            'brandName'              => mb_convert_encoding(trim($this->detail['brandname']), 'UTF-8', 'HTML-ENTITIES'),
            'catEntryId'             => (isset($this->detail['skulist']['sku'][1][0]['catentryid'])) ? $this->detail['skulist']['sku'][1][0]['catentryid']: null,
            'catalogId'              => (isset($this->detail['catalogid'])) ? $this->detail['catalogid'] : null,
            'name'                   => (isset($this->detail['descriptionname'])) ? mb_convert_encoding($this->detail['descriptionname'], 'UTF-8', 'HTML-ENTITIES') : null,
            'shortDescription'       => (isset($this->detail['shortdescription'])) ? mb_convert_encoding($this->detail['shortdescription'], 'UTF-8', 'HTML-ENTITIES') : null,
            'regularPrice'           => (isset($this->detail['regularprice'])) ? (double) $this->detail['regularprice'] : null,
            'salePrice'              => (isset($this->detail['saleprice'])) ? (double) $this->detail['saleprice'] : null,
            'description'            => (isset($this->detail['longdescription'])) ? mb_convert_encoding($this->detail['longdescription'], 'UTF-8', 'HTML-ENTITIES') : null,
            'inStock'                => (isset($this->detail['instock']) && $this->detail['instock'] == true),
            'manufacturerPartNumber' => (isset($this->detail['mfgpartnumber'])) ? $this->detail['mfgpartnumber'] : null,
            'partNumber'             => (isset($this->detail['partnumber'])) ? $this->detail['partnumber'] : null,
            'images'                 => (isset($this->detail['imageurls']['imageurl'][1])) ? $this->detail['imageurls']['imageurl'][1] : array(),
            'ratingCount'            => (isset($this->detail['numreview'])) ? (int) $this->detail['numreview'] : null,
            'rating'                 => (isset($this->detail['rating'])) ? (double) $this->detail['rating'] : null,
            'attributes'             => array(),
            'skuList'                => array(),
            'colorSwatches'          => array(),
            'variants'               => array(),
            'specifications'         => array(),
        );
    }

    protected function setupSpecifications()
    {
        if ( ! $this->isSuccessful())
        {
            return;
        }

        if (isset($this->detail['specifications']['specification'][1]))
        {
            foreach ($this->detail['specifications']['specification'][1] as $spec)
            {
                $newSpec = array(
                    'name'   => trim($spec['label'], ':'),
                    'values' => array(),
                );

                foreach ($spec['attribute'][1] as $value)
                {
                    $pos = strpos($value['value'], ':');
                    $name = substr($value['value'], 0, $pos);
                    $value = substr($value['value'], $pos + 1);

                    $newSpec['values'][] = array(
                        'label' => $name,
                        'value' => $value,
                    );
                }

                $this->product['specifications'][] = $newSpec;
            }
        }
    }

    /**
     * Build out the attributes for product.
     * 
     * @return void
     */
    protected function setupAttributes()
    {
        if ( ! $this->isSuccessful())
        {
            return;
        }

        // Iterate over the product variants array and
        // build out a sane representation of the data.
        if (isset($this->detail['productvariants']['prodlist'][1]))
        {
            foreach ($this->detail['productvariants']['prodlist'][1] as $prod)
            {
                if ( ! isset($prod['product'][1][0]) || ! isset($prod['product'][1][0]['attnames'][1][0]['attname'][1]))
                {
                    continue;
                }

                $prod = $prod['product'][1][0];

                // Build an array of attribute short names.
                $atts = $prod['attnames'][1][0]['attname'][1];

                foreach ($atts as $ai => $av)
                {
                    $atts[$ai] = preg_replace('/[^a-z0-9]+/', '_', strtolower(trim($av)));
                }

                // Is this really just the main product or an actual variant?
                $isMainProduct = (isset($prod['pid']) && $prod['pid'] == $this->product['partNumber'])
                    ? true : false;

                // Iterate over the attributes list.
                foreach ($prod['attnames'][1][0]['attname'][1] as $index => $attName)
                {
                    $attName   = mb_convert_encoding($attName, 'UTF-8', 'HTML-ENTITIES');
                    $shortName = $atts[$index];

                    if ($isMainProduct === true)
                    {
                        // Add attribute to Sane Product array.
                        $this->product['attributes'][$shortName] = array(
                            'name'   => trim($attName),
                            'values' => array(),
                        );

                        // Attach the attribute values.
                        if (isset($prod['prodvarlist']['prodvar']['attlist']['attdata'][1][$index]['avals'][1][0]['aval'][1]))
                        {
                            $this->product['attributes'][$shortName]['values'] = $this->stripQuotes($prod['prodvarlist']['prodvar']['attlist']['attdata'][1][$index]['avals'][1][0]['aval'][1]);
                        }

                        // Attach any found color swatches.
                        if (isset($prod['prodvarlist']['colorswatchlist']['colorswatch'][1]))
                        {
                            foreach ($prod['prodvarlist']['colorswatchlist']['colorswatch'][1] as $swatch)
                            {
                                $this->product['colorSwatches'][mb_convert_encoding($swatch['colorname'], 'UTF-8', 'HTML-ENTITIES')] = $swatch;
                            }
                        }

                        // Build an array of all the skus for each variation
                        // of attribute values.
                        if (isset($prod['prodvarlist']['prodvar']['skulist']['sku'][1]))
                        {
                            foreach ($prod['prodvarlist']['prodvar']['skulist']['sku'][1] as $sku)
                            {
                                $this->product['skuList'][$sku['pid']] = array(
                                    'catEntryId' => $sku['pid'],
                                    'price' => (double) $sku['price'],
                                    'inStock' => ($sku['stk'] == true),
                                    'values'  => array(),
                                );

                                foreach ($sku['avals']['aval'][1] as $avalIndex => $aVal)
                                {
                                    $this->product['skuList'][$sku['pid']]['values'][$atts[$avalIndex]] = $this->stripQuotes($aVal);
                                }
                            }
                        }
                    }
                    else
                    {
                        // Add a product variant to the Sane Product array.
                        $this->product['variants'][$prod['pid']] = array(
                            'name' => $prod['name'],
                            'partNumber' => $prod['pid'],
                            'attributes' => array(),
                            'skuList' => array(),
                            'colorSwatches' => array(),
                        );

                        // Add attribute to Sane Product array as a variant.
                        $this->product['variants'][$prod['pid']]['attributes'][$shortName] = array(
                            'name'   => trim($attName),
                            'values' => array(),
                        );

                        // Attach the attribute values.
                        if (isset($prod['prodvarlist']['prodvar']['attlist']['attdata'][1][$index]['avals'][1][0]['aval'][1]))
                        {
                            $this->product['variants'][$prod['pid']]['attributes'][$shortName]['values'] = $this->stripQuotes($prod['prodvarlist']['prodvar']['attlist']['attdata'][1][$index]['avals'][1][0]['aval'][1]);
                        }

                        // Attach any found color swatches.
                        if (isset($prod['prodvarlist']['colorswatchlist']['colorswatch'][1]))
                        {
                            foreach ($prod['prodvarlist']['colorswatchlist']['colorswatch'][1] as $swatch)
                            {
                                $this->product['variants'][$prod['pid']]['colorSwatches'][mb_convert_encoding($swatch['colorname'], 'UTF-8', 'HTML-ENTITIES')] = $swatch;
                            }
                        }

                        // Build an array of all the skus for each variation
                        // of attribute values.
                        if (isset($prod['prodvarlist']['prodvar']['skulist']['sku'][1]))
                        {
                            foreach ($prod['prodvarlist']['prodvar']['skulist']['sku'][1] as $sku)
                            {
                                $this->product['variants'][$prod['pid']]['skuList'][$sku['pid']] = array(
                                    'catEntryId' => $sku['pid'],
                                    'price' => (double) $sku['price'],
                                    'inStock' => ($sku['stk'] == true),
                                    'values'  => array(),
                                );

                                foreach ($sku['avals']['aval'][1] as $avalIndex => $aVal)
                                {
                                    $this->product['variants'][$prod['pid']]['skuList'][$sku['pid']]['values'][$atts[$avalIndex]] = $this->stripQuotes($aVal);
                                }
                            }
                        }
                    }
                }

                unset($prod);
            }
        }
    }

    protected function stripQuotes($str)
    {
        return str_replace(array('"', '"'), '', $str);
    }

    /**
     * Get Sane product array.
     *
     * @return array
     */
    public function getProduct()
    {
        return $this->product;
    }

}