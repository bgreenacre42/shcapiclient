{
    "name": "HTTP Sears Holdings Company API Client",
    "apiVersion": "2.0.0",
    "operations": {
        "SearchProducts": {
            "httpMethod": "GET",
            "uri": "/shcapi/productsearch",
            "class": "\\Shc\\Api\\Command\\BaseCommand",
            "responseClass": "\\Shc\\Api\\Response\\ProductSearch",
            "summary": "Search for products.",
            "parameters": {
                "keyword": {
                    "location": "query",
                    "type": ["string", "array"],
                    "required": true,
                    "description": "Keyword meant for search."
                },
                "searchType": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "static": true,
                    "default": "keyword",
                    "description": "This parameter will determine the type of search. This could be vertical, keyword, category or subcategory."
                },
                "shopByFilter": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Shop by Filter indicator (values:  Clearance, Sale, Brand)"
                },
                "shopByValue": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "This value is required when shopByFilter=Brand (Ex: shopByFilter=Brand&shopByValue==Sony)."
                },
                "filter": {
                    "location": "query",
                    "type": ["string", "array"],
                    "required": false,
                    "description": "Concatenated filters to refine the result set. (ex: “Brand| Kenmore^Color|Black”)"
                },
                "sortBy": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Sort option for product results. Possible values are UNITS_HIGH_TO_LOW, VIEWS_HIGH_TO_LOW, PRICE_LOW_TO_HIGH, PRICE_HIGH_TO_LOW, RATING_HIGH_TO_LOW, NEWEST, SALE_HIGH_TO_LOW, CLEARANCE_HIGH_TO_LOW, INSTOCK_HIGH_TO_LOW or ORIGINAL_SORT_ORDER."
                },
                "partNumber": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Enumeration of partNumbers to compare. This parameter can have many values."
                },
                "productsOnly": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "1 will return products only not the filters."
                },
                "payThreshhold": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Indicate threshold value based on the product count."
                },
                "startIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "Start Index of the products."
                },
                "endIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "End index of the products."
                }
            }
        },
        "SearchProductsInCategory": {
            "httpMethod": "GET",
            "uri": "/shcapi/productsearch",
            "class": "\\Shc\\Api\\Command\\BaseCommand",
            "responseClass": "\\Shc\\Api\\Response\\ProductSearch",
            "summary": "Search for products.",
            "parameters": {
                "verticalName": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "Selected vertical name. Mandatory when searchType is vertical or category or subcategory."
                },
                "categoryName": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "Selected category name. Mandatory when searchType is category or subcategory."
                },
                "subCategoryName": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "Selected sub category name. Mandatory when searchType is subcategory."
                },
                "searchType": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "static": true,
                    "default": "subcategory",
                    "description": "This parameter will determine the type of search. This could be vertical, keyword, category or subcategory."
                },
                "shopByFilter": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Shop by Filter indicator (values:  Clearance, Sale, Brand)"
                },
                "shopByValue": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "This value is required when shopByFilter=Brand (Ex: shopByFilter=Brand&shopByValue==Sony)."
                },
                "filter": {
                    "location": "query",
                    "type": ["string", "array"],
                    "required": false,
                    "description": "Concatenated filters to refine the result set. (ex: “Brand| Kenmore^Color|Black”)"
                },
                "sortBy": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Sort option for product results. Possible values are UNITS_HIGH_TO_LOW, VIEWS_HIGH_TO_LOW, PRICE_LOW_TO_HIGH, PRICE_HIGH_TO_LOW, RATING_HIGH_TO_LOW, NEWEST, SALE_HIGH_TO_LOW, CLEARANCE_HIGH_TO_LOW, INSTOCK_HIGH_TO_LOW or ORIGINAL_SORT_ORDER."
                },
                "partNumber": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Enumeration of partNumbers to compare. This parameter can have many values."
                },
                "productsOnly": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "1 will return products only not the filters."
                },
                "payThreshhold": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Indicate threshold value based on the product count."
                },
                "startIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "Start Index of the products."
                },
                "endIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "End index of the products."
                }
            }
        },
        "GetProduct": {
            "httpMethod": "GET",
            "uri": "/shcapi/productdetails",
            "class": "\\Shc\\Api\\Command\\BaseCommand",
            "responseClass": "\\Shc\\Api\\Response\\ProductDetail",
            "summary": "Get a products detail object.",
            "parameters": {
                "partNumber": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "The part number of the product."
                },
                "textOnly": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "Indicates whether or not the Short Description and Long Description fields should have all html and script tags removed so that those fields contain only text in the response. Allowed values are 0 (no) and 1 (yes)."
                },
                "showSpec": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Indicates whether or not the specifications (attributes) section should be included in the response XML. Allowed values are false (no) and true (yes). By default, no specifications section is included."
                }
            }
        },
        "GetVerticals": {
            "httpMethod": "GET",
            "uri": "/shcapi/productsearch",
            "class": "\\Shc\\Api\\Command\\VerticalsCommand",
            "summary": "Search for products.",
            "parameters": {}
        },
        "GetCategories": {
            "httpMethod": "GET",
            "uri": "/shcapi/productsearch",
            "class": "\\Shc\\Api\\Command\\BaseCommand",
            "responseClass": "\\Shc\\Api\\Response\\ProductSearch",
            "summary": "Search for categories within a vertical.",
            "parameters": {
                "verticalName": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "Selected vertical name. Mandatory when searchType is vertical or category or subcategory."
                },
                "searchType": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "static": true,
                    "default": "vertical",
                    "description": "This parameter will determine the type of search. This could be vertical, keyword, category or subcategory."
                },
                "shopByFilter": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Shop by Filter indicator (values:  Clearance, Sale, Brand)"
                },
                "shopByValue": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "This value is required when shopByFilter=Brand (Ex: shopByFilter=Brand&shopByValue==Sony)."
                },
                "filter": {
                    "location": "query",
                    "type": ["string", "array"],
                    "required": false,
                    "description": "Concatenated filters to refine the result set. (ex: “Brand| Kenmore^Color|Black”)"
                },
                "sortBy": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Sort option for product results. Possible values are UNITS_HIGH_TO_LOW, VIEWS_HIGH_TO_LOW, PRICE_LOW_TO_HIGH, PRICE_HIGH_TO_LOW, RATING_HIGH_TO_LOW, NEWEST, SALE_HIGH_TO_LOW, CLEARANCE_HIGH_TO_LOW, INSTOCK_HIGH_TO_LOW or ORIGINAL_SORT_ORDER."
                },
                "partNumber": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Enumeration of partNumbers to compare. This parameter can have many values."
                },
                "productsOnly": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "1 will return products only not the filters."
                },
                "payThreshhold": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Indicate threshold value based on the product count."
                },
                "startIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "Start Index of the products."
                },
                "endIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "End index of the products."
                }
            }
        },
        "GetSubCategories": {
            "httpMethod": "GET",
            "uri": "/shcapi/productsearch",
            "class": "\\Shc\\Api\\Command\\BaseCommand",
            "responseClass": "\\Shc\\Api\\Response\\ProductSearch",
            "summary": "Search for sub categories within a vertical within a category.",
            "parameters": {
                "verticalName": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "Selected vertical name. Mandatory when searchType is vertical or category or subcategory."
                },
                "categoryName": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "description": "Selected category name. Mandatory when searchType is category or subcategory."
                },
                "searchType": {
                    "location": "query",
                    "type": "string",
                    "required": true,
                    "static": true,
                    "default": "category",
                    "description": "This parameter will determine the type of search. This could be vertical, keyword, category or subcategory."
                },
                "shopByFilter": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Shop by Filter indicator (values:  Clearance, Sale, Brand)"
                },
                "shopByValue": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "This value is required when shopByFilter=Brand (Ex: shopByFilter=Brand&shopByValue==Sony)."
                },
                "filter": {
                    "location": "query",
                    "type": ["string", "array"],
                    "required": false,
                    "description": "Concatenated filters to refine the result set. (ex: “Brand| Kenmore^Color|Black”)"
                },
                "sortBy": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Sort option for product results. Possible values are UNITS_HIGH_TO_LOW, VIEWS_HIGH_TO_LOW, PRICE_LOW_TO_HIGH, PRICE_HIGH_TO_LOW, RATING_HIGH_TO_LOW, NEWEST, SALE_HIGH_TO_LOW, CLEARANCE_HIGH_TO_LOW, INSTOCK_HIGH_TO_LOW or ORIGINAL_SORT_ORDER."
                },
                "partNumber": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Enumeration of partNumbers to compare. This parameter can have many values."
                },
                "productsOnly": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "1 will return products only not the filters."
                },
                "payThreshhold": {
                    "location": "query",
                    "type": "string",
                    "required": false,
                    "description": "Indicate threshold value based on the product count."
                },
                "startIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "Start Index of the products."
                },
                "endIndex": {
                    "location": "query",
                    "type": "integer",
                    "required": false,
                    "description": "End index of the products."
                }
            }
        }
    }
}