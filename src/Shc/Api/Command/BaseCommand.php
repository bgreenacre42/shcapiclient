<?php namespace Shc\Api\Command;

use Guzzle\Service\Command\OperationCommand;

class BaseCommand extends OperationCommand {

    /**
     * Filter specific keys.
     * 
     * @param  string $key Key to get.
     * @return string
     */
    public function get($key)
    {
        $item = parent::get($key);

        // If the value is an array then filtering is needed.
        if (is_array($item))
        {
            switch($key)
            {
                case 'filter':
                    // filter key has specific concatenation
                    // for associative array.
                    $compiled = '';

                    foreach ($item as $name => $value)
                    {
                        $compiled .= trim($name) . '|';

                        if (is_array($value))
                        {
                            $value = implode('-', $value);
                        }

                        $compiled .= $value . '^';
                    }

                    $compiled = rtrim($compiled, '^');

                    $item = $compiled;

                    break;
                default:
                    // Concat with a "+" character.
                    $item = implode('+', $item);

                    break;
            }
        }

        return $item;
    }

    protected function build()
    {
        parent::build();

        $query = $this->request->getQuery();

        if ($this['store'])
        {
            $query->set('store', $this['store']);
        }
    }

}