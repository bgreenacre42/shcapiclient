<?php namespace Shc\Api\Command;

use Guzzle\Service\Command\OperationCommand;

class VerticalsCommand extends BaseCommand {

    public function build()
    {
        switch(strtolower(trim($this['store'])))
        {
            case 'kmart':
                $url = 'http://www.kmart.com';
                $path = '/shc/s/smv_10151_10104';

                break;
            case 'sears':
            default:
                $url = 'http://www.sears.com';
                $path = '/shc/s/smv_10153_12605';

                break;
        }

        $this->request = $this->client->get($path);

        $this->request->setUrl($url . $path);
    }

    public function process()
    {
        $verticals = array();
        $body = (string) $this->request->getResponse()->getBody();

        $body = preg_split('/\<\s?div.*?class=(?:\'|")siteMapContainer(?:\'|")\>/', $body);
        preg_match_all('/\<\s?a.*?href=(?:\'|")([^\'"]+).*?\>(.*?)\<\/a\>/', $body[1], $matches);

        if (is_array($matches))
        {
            foreach ($matches[1] as $key => $uri)
            {
                preg_match('/\/?([^\/]+)\/b-/', $uri, $match);

                if ( ! empty($match))
                {
                    $verticals[] = array(
                        'term' => html_entity_decode(trim($matches[2][$key])),
                        'slug' => $match[1],
                    );
                }
            }
        }

        $this->result = $verticals;
    }

}