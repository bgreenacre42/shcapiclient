<?php namespace Shc\Api\Model;

use Guzzle\Service\Resource\ResourceIterator;

class SearchProductsIterator extends ResourceIterator {

    protected $total;
    protected $indexDiff = 0;
    protected $recordsFound = 0;
    protected $maxIndex = 1000;
    protected $limitRecords = 100;
    protected $priceRange = array(0, 50);
    protected $noResultsCount = 0;
    protected $highestPrice;
    protected $products;

    protected function sendRequest()
    {
        $count = 0;

        do {
            if ($count > 0)
            {
                $this->command = clone $this->originalCommand;
            }

            $isDone = false;

            if ($this->nextToken)
            {
                $this->command->set('startIndex', $this->nextToken['start']);
                $this->command->set('endIndex', $this->nextToken['end']);

                if (isset($this->nextToken['filter']))
                {
                    $this->command->set('filter', $this->nextToken['filter']);
                }
            }
            else
            {
                $this->command->set('startIndex', 0);
                $this->command->set('endIndex', $this->limitRecords);
            }

            $this->command->set('sortBy', 'PRICE_HIGH_TO_LOW');

            $result = $this->command->execute();

            $this->products = &$result->getProducts();

            if (is_null($this->total))
            {
                $this->total = $result->getTotalCount();
            }

            if (is_null($this->highestPrice) && isset($this->products[0]['displayprice']))
            {
                $this->highestPrice = (double) $this->products[0]['displayprice'];

                $this->setPriceRange($this->highestPrice - 50, $this->highestPrice);
            }

            if ($result->getTotalCount() == 0 || empty($this->products))
            {
                ++$this->noResultsCount;
            }

            if ($this->indexDiff > 0)
            {
                $this->indexDiff -= $result->getTotalCount();

                if ($this->indexDiff > 0)
                {
                    $isDone = false;
                }
                elseif ($this->indexDiff < 0)
                {
                    $isDone             = true;
                    $this->products     = array_slice($this->products, $this->indexDiff);
                    $this->indexDiff    = 0;
                    $this->recordsFound += count($this->products);
                }
            }
            else
            {
                $isDone = true;
                $this->recordsFound += count($this->products);
            }

            if ($this->total >= $this->maxIndex)
            {
                $nextToken = array();
                $start     = (int) $this->command->get('startIndex');
                $end       = (int) $this->command->get('endIndex');

                $newStart = $start + $this->limitRecords;

                if (isset($this->nextToken['filter']) && $result->getTotalCount() > $this->maxIndex)
                {
                    $this->autoAdjustPriceRange();
                }

                if ($start >= $result->getTotalCount())
                {
                    $isDone             = false;
                    $this->indexDiff    = $start - $result->getTotalCount();
                    $this->recordsFound -= $this->indexDiff;
                }

                if ($newStart >= $this->maxIndex)
                {
                    if ($end >= $this->maxIndex)
                    {
                        $nextToken = array(
                            'start' => 0,
                            'end'   => $this->limitRecords,
                        );

                        if (is_null($this->highestPrice))
                        {
                            $isDone = ($this->autoIncreasePriceRange($result->getTotalCount()) === false);
                        }
                        else
                        {
                            $isDone = ($this->autoDecreasePriceRange($result->getTotalCount()) === false);
                        }
                    }
                    else
                    {
                        $nextToken = array(
                            'start' => $end + 1,
                            'end'   => $this->maxIndex,
                        );
                    }
                }
                elseif ($newStart >= $result->getTotalCount())
                {
                    $totalCount = $result->getTotalCount();

                    if ($end >= $totalCount)
                    {
                        $nextToken = array(
                            'start' => 0,
                            'end'   => $this->limitRecords,
                        );

                        if (is_null($this->highestPrice))
                        {
                            $isDone = ($this->autoIncreasePriceRange($result->getTotalCount()) === false);
                        }
                        else
                        {
                            $isDone = ($this->autoDecreasePriceRange($result->getTotalCount()) === false);
                        }
                    }
                    else
                    {
                        $nextToken = array(
                            'start' => $end + 1,
                            'end'   => $totalCount,
                        );
                    }
                }
                else
                {
                    $nextToken = array(
                        'start' => $start + $this->limitRecords + 1,
                        'end'   => ($start + $this->limitRecords + 1) + $this->limitRecords,
                    );
                }

                $nextToken['filter'] = 'price|' . implode('-', $this->priceRange);

                $this->nextToken = $nextToken;
            }
            else
            {
                $this->nextToken = array(
                    'start' => ( (int) $this->command->get('startIndex') + $this->limitRecords + 1),
                    'end'   => ( (int) ($this->command->get('startIndex') + $this->limitRecords + 1) + $this->limitRecords),
                );
            }

            ++$count;

            unset($result);

            // $isDone = true;
        } while($isDone === false);

        return ($this->recordsFound >= $this->total)
            ? array()
            : $this->products;
    }

    protected function autoAdjustPriceRange()
    {
        $low  = $this->priceRange[0];
        $high = $this->priceRange[1];

        $adjustment = ($high - $low) * 0.3;

        $this->setPriceRange($low, $high - $adjustment);
    }

    protected function autoDecreasePriceRange($totalCount)
    {
        $low  = $this->priceRange[0];
        $high = $this->priceRange[1];

        $difference = $high - $low;

        if ($this->noResultsCount >= 5 && $totalCount <= $this->maxIndex)
        {
            $this->noResultsCount = 0;

            if ($this->highestPrice > 10000)
            {
                $difference = 1000;
            }
            elseif ($this->highestPrice <= 10000 && $this->highestPrice > 1000)
            {
                $difference = 200;
            }
            else
            {
                $difference = 50;
            }
        }
        elseif ($totalCount <= $this->maxIndex && $difference <= 10)
        {
            $difference = 20;
        }

        $this->setPriceRange($low - $difference, $low);

        if ($low <= 0)
        {
            return false;
        }

        return true;
    }

    protected function autoIncreasePriceRange($totalCount)
    {
        $low  = $this->priceRange[0];
        $high = $this->priceRange[1];

        $difference = $high - $low;

        if ($this->noResultsCount >= 5 && $totalCount <= $this->maxIndex)
        {
            $this->noResultsCount = 0;
            $difference = 100;
        }
        elseif ($totalCount <= $this->maxIndex && $difference <= 10)
        {
            $difference = 20;
        }

        $this->setPriceRange($high, $high + $difference);

        if ($high > 10000)
        {
            return false;
        }

        return true;
    }

    public function setRecordLimit($limit)
    {
        if ($limit >= $this->maxIndex)
        {
            throw \InvalidArgumentException(
                sprintf(
                    'Search products limit must be lower than %d',
                    $this->maxIndex
                )
            );
        }

        $this->limitRecords = (int) $limit;

        return $this;
    }

    public function getRecordLimit()
    {
        return $this->limitRecords;
    }

    public function setMaxIndex($max)
    {
        $this->maxIndex = (int) $max;

        return $this;
    }

    public function getMaxIndex()
    {
        return $this->maxIndex;
    }

    public function setPriceRange($low, $high)
    {
        $this->priceRange = array(
            number_format($low, 2, '.', ''),
            number_format($high, 2, '.', ''),
        );

        return $this;
    }

    public function getPriceRange()
    {
        return $this->priceRange;
    }

}