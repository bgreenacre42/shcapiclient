<?php namespace Shc\Api;

use Guzzle\Common\Collection;
use Guzzle\Service\Client;
use Guzzle\Service\Description\ServiceDescription;

class ShcClient extends Client {

    /**
     * Create an object with required configs.
     * 
     * @param  array  $config Associative array of config values to pass into command object.
     * @return object         Object of this class.
     */
    public static function factory($config = array())
    {
        $default = array(
            'base_url'    => 'http://webservices.sears.com/shcapi/',
            'store'       => 'Sears',
            'catalogId'   => '12605',
            'contentType' => 'json',
            'curl.options' => array(
                CURLOPT_CONNECTTIMEOUT => 30,
                CURLOPT_TIMEOUT => 120,
                'body_as_string' => true,
            ),
            'command.disable_validation' => true,
        );

        $required = array('base_url', 'appID', 'authID', 'apikey', 'store', 'contentType');
        $config = Collection::fromConfig($config, $default, $required);
        $description = ServiceDescription::factory(__DIR__.'/service.json');

        $client = new self($config->get('base_url'), $config);
        $client->setDescription($description);

        return $client;
    }

    /**
     * Set the default/required query string fields
     * for all requests made.
     * 
     * @param  string $method  HTTP method.
     * @param  string $uri     path for request
     * @param  array  $headers HTTP headers.
     * @param  string $body    Body of request.
     * @param  array  $options Any other options.
     * @return obeject         Request object.
     */
    public function createRequest($method = RequestInterface::GET, $uri = null, $headers = null, $body = null, array $options = array())
    {
        $request = parent::createRequest($method, $uri, $headers, $body, $options);
        $config  = $this->getConfig();
        $query   = $request->getQuery();

        foreach ($config->getAll(array('appID', 'authID', 'apikey', 'store', 'catalogId', 'contentType')) as $name => $value)
        {
            $query->set($name, $value);
        }

        return $request;
    }

}